package pt.guilhermesilva.marvelmadness.utils;

import android.os.AsyncTask;

import java.io.File;
import java.util.List;

/**
 * Simple class to check the list of custom covers in the dropbox.
 * Created by guilhermesilva on 26/03/16.
 */
public class CheckAvailableCoversTask extends AsyncTask<Void, Void, List<String>> {
    private final Callback mCallback;

    public interface Callback {
        void onDownloadComplete(List<String> result);
        void onError(Exception e);
    }

    public CheckAvailableCoversTask(Callback callback){
        this.mCallback = callback;
    }

    @Override
    protected List<String> doInBackground(Void... params) {
        return DropboxUtils.sInstance.getListOfCustomCoverImages();
    }

    @Override
    protected void onPostExecute(List<String> result) {
        super.onPostExecute(result);
        mCallback.onDownloadComplete(result);

    }
}
