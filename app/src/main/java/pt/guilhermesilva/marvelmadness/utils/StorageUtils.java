package pt.guilhermesilva.marvelmadness.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by guilhermesilva on 26/03/16.
 */
public class StorageUtils {
    final static String TAG = StorageUtils.class.getSimpleName();

    public static boolean existsBitmap(Context context, String comicId) {
        //Get the text file
        File file = new File(getDeviceStoragePath(context), String.valueOf(comicId));
        return file.exists();
    }

    /**
     * Returns the fiel containing the bitmap for the comic id
     * @param context
     * @param comicId
     * @return
     */
    public static File retrieveBitmap(Context context, String comicId) {

        //Get the text file
        File file = new File(getDeviceStoragePath(context), String.valueOf(comicId));

        //Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
            Log.d(TAG, "There was a problem reading the file " + e.getMessage());
        }
        return file;
    }

    /**
     * Saves the bitmap in the cache dir
     * @param context
     * @param comicId
     * @param bitmap
     */
    public static void saveBitmap(Context context, String comicId, Bitmap bitmap) {
        String path = getDeviceStoragePath(context).toString();
        OutputStream fOut = null;
        File file = new File(path, String.valueOf(comicId)); // the File to save to
        try {
            fOut = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush();
            fOut.close(); // do not forget to close the stream

            MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes the bitmap from the cache.
     * @param context
     * @param comicId
     * @return
     */
    public static boolean deleteBitmap(Context context, String comicId) {
        //Get the text file
        File file = new File(getDeviceStoragePath(context), String.valueOf(comicId));
        return file.delete();
    }

    /**
     * Gets the storage path to all the cache to store images
     * @param context
     * @return
     */
    public static File getDeviceStoragePath(Context context) {
        return Environment.getExternalStorageDirectory();
        //return context.getCacheDir();
    }
}
