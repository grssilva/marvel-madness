package pt.guilhermesilva.marvelmadness.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.text.StaticLayout;
import android.util.Log;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuth;
import com.dropbox.core.android.Auth;
import com.dropbox.core.http.OkHttpRequestor;
import com.dropbox.core.util.StringUtil;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.users.FullAccount;
import com.dropbox.core.v2.users.Name;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Class to show the simpleton implementation and a utils for the dropboc functionalities.
 * Created by guilhermesilva on 24/03/16.
 */
public class DropboxUtils {
    public static final String SHARED_PREFERENCES_TAG = "marvel_madness";

    public static final String SHARED_PREFERENCES_ACCESS_TOKEN = "access_token";

    public static DropboxUtils sInstance = new DropboxUtils();
    private DbxClientV2 mClient;
    private Map<String, Metadata> mComicsWithCostumCovers = new HashMap<>();

    /**
     * Initializes the dropbox utils
     *
     * @param accessToken
     */
    public void init(String accessToken) {
        if (mClient == null) {
            String userLocale = Locale.getDefault().toString();
            DbxRequestConfig requestConfig = new DbxRequestConfig(
                    "marvel-madness",
                    userLocale);
            mClient = new DbxClientV2(requestConfig, accessToken);
        }
    }

    /**
     * Called on the acivity on resume function
     * @param context
     */
    public void onResume(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCES_TAG, context.MODE_PRIVATE);
        String accessToken = prefs.getString(SHARED_PREFERENCES_ACCESS_TOKEN, null);
        if (accessToken == null) {
            accessToken = Auth.getOAuth2Token();
            if (accessToken != null) {
                prefs.edit().putString(SHARED_PREFERENCES_ACCESS_TOKEN, accessToken).apply();
                DropboxUtils.sInstance.init(accessToken);
            }
        } else {
            DropboxUtils.sInstance.init(accessToken);
        }
    }

    /**
     * Checks if any user is logged in
     *
     * @return true if the user is logged in
     */
    public boolean isUserLoggedIn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCES_TAG, context.MODE_PRIVATE);
        String accessToken = prefs.getString(SHARED_PREFERENCES_ACCESS_TOKEN, null);
        return accessToken != null;
    }

    /**
     * Small function to get the user name
     *
     * @return returns the name of the user or null is the user is not logged in.
     */
    public String getUserName() {
        try {
            String name = mClient.users().getCurrentAccount().getName().getDisplayName();
            return name;
        } catch (DbxException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }

        return null;
    }

    /**
     * Logs the user out of the dropbox account
     * @param context
     */
    public void logOut(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFERENCES_TAG, context.MODE_PRIVATE);
        prefs.edit().remove(SHARED_PREFERENCES_ACCESS_TOKEN).apply();
    }

    /**
     * Get the list of custom cover images in the app
     *
     * @return an array with the list of ids with custom covers
     */
    public List<String> getListOfCustomCoverImages() {
        List<String> customCoverIds = new ArrayList<>();
        try {
            ListFolderResult result = mClient.files().listFolder("");
            for (Metadata metadata : result.getEntries()) {
                mComicsWithCostumCovers.put(metadata.getName(), metadata);
                customCoverIds.add(metadata.getName());
            }
        } catch (DbxException e) {
            e.printStackTrace();
        }

        return customCoverIds;
    }

    /**
     * Add a new comic book cover to the dropbox accound
     */
    public void insertCoverImage(Context context, String comicId, Bitmap bitmap) {
        UploadFileTask task = new UploadFileTask(context, mClient, new UploadFileTask.Callback() {
            @Override
            public void onUploadComplete(FileMetadata result) {
                Log.d("comid", "yes");
            }

            @Override
            public void onError(Exception e) {
                Log.d("comid", "no");

            }
        });

        File f = bitmapToFile(context, bitmap, comicId + "");
        task.execute(f);
    }

    /**
     * Covertes a bitmap into a file.
     * @param context
     * @param bitmap
     * @param filename
     * @return
     */
    public File bitmapToFile(Context context, Bitmap bitmap, String filename) {
        //create a file to write bitmap data
        File f = new File(context.getCacheDir(), filename);
        try {
            f.createNewFile();


            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    /**
     * Deletes a cover image from the dropbox account
     */
    public void deleteCoverImage(String comicId) {
        //Function missing from the dropbox documentation.
    }

    /**
     * Gets the url of the commic book custom cover image
     *
     * @return the url of the image or null if the image does not exist
     */
    public void getCoverImageBitmap(Context context, String comidId, DownloadFileTask.Callback callback) {
        DownloadFileTask task = new DownloadFileTask(context, mClient, callback);
        Metadata md = mComicsWithCostumCovers.get(String.valueOf(comidId));

        for (Map.Entry<String, Metadata> entry : mComicsWithCostumCovers.entrySet()) {
            Log.w("comics", String.valueOf(comidId) + " metadata? " + entry.getKey().equals(String.valueOf(comidId)));
        }

        FileMetadata metadata = (FileMetadata) md;

        if (metadata != null) {
            Log.w("comics", "metadata for " + comidId + " is not null");
            task.execute(metadata);
        } else {
            Log.e("comics", "metadata for " + comidId + " is null");
        }
    }
}
