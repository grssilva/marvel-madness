package pt.guilhermesilva.marvelmadness.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pt.guilhermesilva.marvelmadness.R;
import pt.guilhermesilva.marvelmadness.models.Comic;
import pt.guilhermesilva.marvelmadness.utils.DownloadFileTask;
import pt.guilhermesilva.marvelmadness.utils.DropboxUtils;
import pt.guilhermesilva.marvelmadness.utils.StorageUtils;

/**
 * Adapter for the comic book images in the Main activity
 * Created by guilhermesilva on 25/03/16.
 */
public class ComicBookAdapter extends ArrayAdapter<Comic> {
    private LayoutInflater mInflater;

    /**
     * List of comics with custom covers
     */
    private List<String> mComicsWithCustomCovers;

    public ComicBookAdapter(Context context) {
        super(context, R.layout.view_comic_book, new ArrayList<Comic>());
        mInflater = LayoutInflater.from(context);
        mComicsWithCustomCovers = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Comic comic = getItem(position);
        View view;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_comic_book, parent, false);
        } else {
            view = convertView;
        }

        final ImageView imageView = (ImageView) view.findViewById(R.id.grid_item_image);

        // check if we have the image stored in the device
        if (StorageUtils.existsBitmap(getContext(), comic.getId())) {
            File file = StorageUtils.retrieveBitmap(getContext(), comic.getId());
            Picasso.with(getContext()).load(file).into(imageView);
        } else {
            // Check if we have more than one image
            if (comic.getImageLinks().size() > 0) {
                String imagePath = comic.getImageLinks().get(0);
                Picasso.with(getContext()).load(imagePath + "/portrait_uncanny.jpg").into(imageView);

            }

            //check we we have the image storage on the dropbox and not on the device
            if (mComicsWithCustomCovers.size() > 0) {
                if (mComicsWithCustomCovers.contains(String.valueOf(comic.getId()))) {
                    DropboxUtils.sInstance.getCoverImageBitmap(getContext(), comic.getId(), new DownloadFileTask.Callback() {
                        @Override
                        public void onDownloadComplete(File result) {
                            Picasso.with(getContext()).load(result).into(imageView);
                        }

                        @Override
                        public void onError(Exception e) {
                        }
                    });
                }
            }
        }

        return view;
    }

    /**
     * Helper function to set the custom comic book covers
     * @param mComicsWithCustomCovers
     */
    public void setComicsWithCustomCovers(List<String> mComicsWithCustomCovers) {
        this.mComicsWithCustomCovers = mComicsWithCustomCovers;
        this.notifyDataSetChanged();
    }
}
