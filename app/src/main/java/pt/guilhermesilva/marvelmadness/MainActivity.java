package pt.guilhermesilva.marvelmadness;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dropbox.core.android.Auth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pt.guilhermesilva.marvelmadness.adapters.ComicBookAdapter;
import pt.guilhermesilva.marvelmadness.models.Comic;
import pt.guilhermesilva.marvelmadness.utils.CheckAvailableCoversTask;
import pt.guilhermesilva.marvelmadness.utils.DropboxUtils;
import pt.guilhermesilva.marvelmadness.utils.Utils;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private ComicBookAdapter mAdapter;
    private int mCurrentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AbsListView comicsContainerView = (AbsListView) findViewById(R.id.comic_list_view);

        mAdapter = new ComicBookAdapter(getBaseContext());
        comicsContainerView.setAdapter(mAdapter);
        comicsContainerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Comic comic = mAdapter.getItem(position);

                Intent intent = new Intent(MainActivity.this, ComicBookActivity.class);
                intent.putExtra(ComicBookActivity.PARAM_COMIC, comic);
                startActivity(intent);
            }
        });

        mAdapter.clear();
        mCurrentPage = 0;
        comicsContainerView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                    // End has been reached
                    getComics(++mCurrentPage);
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });


        getComics(0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                int permsRequestCode = 200;
                requestPermissions(perms, permsRequestCode);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        DropboxUtils.sInstance.onResume(getBaseContext());

        mAdapter.notifyDataSetChanged();

        updateLoginButton();
    }

    /**
     * Updates the login button to show either the login or logout message
     */
    private void updateLoginButton() {
        Button loginButton = (Button) findViewById(R.id.login_button);

        if (!DropboxUtils.sInstance.isUserLoggedIn(getBaseContext())) {
            loginButton.setText(R.string.login_with_dropbox);
        } else {
            String name = DropboxUtils.sInstance.getUserName();
            if (name != null) {
                String text = getString(R.string.welcome_user, name);
                loginButton.setText(text);
            } else {
                loginButton.setText(R.string.welcome_user_simple);
            }

            //loginButton.setVisibility(View.GONE);
        }
    }

    private void getComics(int page) {
        Toast.makeText(getBaseContext(), "Getting the comic list", Toast.LENGTH_SHORT).show();

        int limit = 20;
        int offSet = limit * page;
        long timestamp = System.currentTimeMillis();
        String publicKey = getString(R.string.public_key);
        String privateKey = getString(R.string.private_key);

        String key = timestamp + privateKey + publicKey;
        String hashedKey = Utils.md5(key);

        String baseUrl = getString(R.string.api_get_comics);
        String url = String.format(baseUrl, timestamp, publicKey, hashedKey, limit, offSet);

        Log.d(TAG, "Getting the comics");
        Log.d(TAG, "Getting the comics for url " + url);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Got the comics correctly" + response.toString());

                        List<Comic> comics = parseComics(response);

                        Log.d(TAG, "Got " + comics.size() + " comics correctly");

                        mAdapter.addAll(comics);
                        mAdapter.notifyDataSetChanged();

                        if (DropboxUtils.sInstance.isUserLoggedIn(getBaseContext())) {
                            getListOfCustomCovers();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Did not get the  comics correctly " + error.getMessage());
                    }
                });

        Volley.newRequestQueue(this).add(jsObjRequest);
    }

    public void getListOfCustomCovers() {
        Toast.makeText(getBaseContext(), "Getting list of custom comic covers", Toast.LENGTH_SHORT).show();

        CheckAvailableCoversTask task = new CheckAvailableCoversTask(new CheckAvailableCoversTask.Callback() {
            @Override
            public void onDownloadComplete(List<String> result) {
                setListOfCustomCovers(result);
            }

            @Override
            public void onError(Exception e) {

            }
        });

        task.execute();
    }

    public void setListOfCustomCovers(List<String> comics) {
        Toast.makeText(getBaseContext(), "Setting list of comic covers", Toast.LENGTH_SHORT).show();
        mAdapter.setComicsWithCustomCovers(comics);

    }

    /**
     * Parses the json int and array of comics
     *
     * @param jsonObject
     * @return
     */
    private List<Comic> parseComics(JSONObject jsonObject) {
        List<Comic> comics = new ArrayList<>();

        try {
            JSONArray comicJSONArray = jsonObject.getJSONObject("data").getJSONArray("results");
            int arrayLength = comicJSONArray.length();
            for (int i = 0; i < arrayLength; ++i) {
                Comic comic = Comic.parse(comicJSONArray.getJSONObject(i));
                if (comic != null) {
                    comics.add(comic);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return comics;
    }

    /**
     * Event for when the user clicks on the login button
     *
     * @param view
     */
    public void loginWithDropboxButtonClick(View view) {
        if (!DropboxUtils.sInstance.isUserLoggedIn(getBaseContext())) {
            Log.d(TAG, "Logging in");
            Auth.startOAuth2Authentication(MainActivity.this, getString(R.string.app_key));
        } else {
            Log.d(TAG, "Logging out");
            Toast.makeText(getBaseContext(), R.string.you_logged_out, Toast.LENGTH_SHORT).show();
            DropboxUtils.sInstance.logOut(getBaseContext());
            updateLoginButton();
        }
    }
}
