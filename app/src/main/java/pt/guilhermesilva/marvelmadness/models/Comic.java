package pt.guilhermesilva.marvelmadness.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple model for the class Comic. It has the hability to parse form json and from a parcel.
 * Created by guilhermesilva on 24/03/16.
 */
public class Comic implements Parcelable{
    /**
     * Id of the comic as retrived by the json
     */
    private String mId;
    /**
     * Title of the comic
     */
    private String mTitle;

    /**
     * List of images in the comic. Only the first one is required in the app.
     */
    private List<String> mImageUrls;

    public Comic() {
        mImageUrls = new ArrayList<>();
    }

    private Comic(Parcel in) {
        mImageUrls = new ArrayList<>();

        mId = in.readString();
        mTitle = in.readString();
        in.readStringList(mImageUrls);
    }

    /**
     *
     * @return the id of the comic
     */
    public String getId() {
        return mId;
    }

    /**
     *
     * @return title of the comic
     */
    public String getTitle() {
        return mTitle;
    }

    /**
     *
     * @return base links for the image urls
     */
    public List<String> getImageLinks() {
        return mImageUrls;
    }

    /**
     * Function used to parse a JSONObject into a Comic object.
     * @param jsonObject
     * @return
     */
    public static Comic parse(JSONObject jsonObject){
        Comic comic = new Comic();
        try {
            comic.mId = jsonObject.getString("id");
            comic.mTitle = jsonObject.getString("title");
            JSONArray imageArray = jsonObject.getJSONArray("images");
            int imageArrayLength = imageArray.length();

            if(imageArrayLength == 0){
                //Let's discart this comic if it doesn't have images!
                return null;
            }

            for (int i =0 ; i< imageArrayLength; ++i) {
                JSONObject imageObject = imageArray.getJSONObject(i);
                String imagePath = imageObject.getString("path");
                comic.mImageUrls.add(imagePath);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return comic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mTitle);
        dest.writeStringList(mImageUrls);
    }

    public static final Parcelable.Creator<Comic> CREATOR
            = new Parcelable.Creator<Comic>() {
        public Comic createFromParcel(Parcel in) {
            return new Comic(in);
        }

        public Comic[] newArray(int size) {
            return new Comic[size];
        }
    };


}
