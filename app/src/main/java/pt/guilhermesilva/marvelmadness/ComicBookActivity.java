package pt.guilhermesilva.marvelmadness;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import pt.guilhermesilva.marvelmadness.models.Comic;
import pt.guilhermesilva.marvelmadness.utils.DropboxUtils;
import pt.guilhermesilva.marvelmadness.utils.StorageUtils;
import pt.guilhermesilva.marvelmadness.utils.UploadFileTask;

/**
 * Base activity to show a comic book.
 */
public class ComicBookActivity extends AppCompatActivity {

    public static final String PARAM_COMIC = "comic";
    private static final int REQUEST_CODE_GET_IMAGE_FROM_GALLERY = 1;
    private static final int REQUEST_CODE_GET_IMAGE_FROM_CAMERA = 2;
    private Comic mComic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic_book);
        mComic = getIntent().getExtras().getParcelable(PARAM_COMIC);
        setTitle(mComic.getTitle());
        updateImage();
    }

    /**
     * Update the image in the layout
     */
    public void updateImage() {
        ImageView imageView = (ImageView) findViewById(R.id.image);
        View deleteButton = findViewById(R.id.delete_button);
        if(StorageUtils.existsBitmap(getBaseContext(), mComic.getId())) {
            File file = StorageUtils.retrieveBitmap(getBaseContext(), mComic.getId());
            Picasso.with(getBaseContext()).load(file).into(imageView);
            deleteButton.setEnabled(true);
        }else {
            if(mComic.getImageLinks().size() >0 ) {
                String imagePath = mComic.getImageLinks().get(0);
                Picasso.with(getBaseContext()).load(imagePath + "/portrait_uncanny.jpg").into(imageView);
            }
            deleteButton.setEnabled(false);
        }
    }

    /**
     * Retrieve image from gallery
     */
    private void getPictureFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_CODE_GET_IMAGE_FROM_GALLERY);
    }

    /**
     * Retrieve image from camera
     */
    private void getImageFromCamera() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_CODE_GET_IMAGE_FROM_CAMERA);
        } else {
            Toast.makeText(getBaseContext(), R.string.your_device_does_not_support_camera, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap = null;
        if (requestCode == REQUEST_CODE_GET_IMAGE_FROM_GALLERY && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            bitmap = BitmapFactory.decodeFile(picturePath);
        }
         else if (requestCode == REQUEST_CODE_GET_IMAGE_FROM_CAMERA && resultCode == Activity.RESULT_OK) {

            Bundle extras = data.getExtras();
            bitmap = (Bitmap) extras.get("data");
        }

        if(bitmap != null) {

            Bitmap finalBitmap = resizeBitmap(bitmap);
            ImageView imageView = (ImageView) findViewById(R.id.image);
            imageView.setImageBitmap(finalBitmap);
            saveFile(mComic.getId(), finalBitmap);

            updateImage();
        }
    }

    /**
     * Save cover in teh device cash and in the dropbox
     * @param comicId
     * @param bitmap
     */
    public void saveFile(String comicId, Bitmap bitmap) {
        DropboxUtils.sInstance.insertCoverImage(getBaseContext(), comicId, bitmap);
        StorageUtils.saveBitmap(getBaseContext(), comicId, bitmap);
    }

    /**
     * Event for the gallery button
     * @param view
     */
    public void galleryButtonClick(View view) {
        getPictureFromGallery();
    }

    /**
     * Event for the camera button
     * @param view
     */
    public void cameraButtonClick(View view) {
        getImageFromCamera();
    }

    /**
     * Event for the delete button
     * @param view
     */
    public void deleteButton(View view) {
        StorageUtils.deleteBitmap(getBaseContext(), mComic.getId());
        updateImage();
    }

    /**
     * Resize the image to improve space
     * @param bitmap
     * @return
     */
    private Bitmap resizeBitmap(Bitmap bitmap){
        return Bitmap.createScaledBitmap(bitmap, 300, 450, false);
    }
}
